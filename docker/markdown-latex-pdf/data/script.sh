#!/bin/bash

input_file='example.md'
output_file='example.pdf'
latex_template='eisvogel.tex'

if [[ -n $1 ]]; then
        input_file=$1

        if [[ -n $2 ]]; then
                output_file=$2

                if [[ -n $3 ]]; then
                        latex_template=$3
                fi
        fi
fi

pandoc $input_file -o $output_file --from markdown --template $latex_template --listings
